require_relative 'router'

class Trip
  def initialize(origin, destination)
    @origin = origin
    @destination = destination
  end

  # Returns an Integer, so it truncates any decimal
  def duration
    duration_in_seconds = Router.new(@origin, @destination).duration_in_seconds
    if duration_in_seconds.nil?
      nil
    else
      Integer(duration_in_seconds / 60)
    end
  end
end
