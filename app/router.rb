require 'faraday'
require 'json'
require_relative 'router_response_json_parser'

class Router
  HERE_URL = 'https://router.hereapi.com/v8/routes'.freeze
  HERE_PARAMS = "?transportMode=car&return=summary&apikey=#{ENV['HERE_API_KEY']}".freeze

  def initialize(origin, destination)
    string_origin = "#{origin.latitude}%2C#{origin.longitude}"
    string_destination = "#{destination.latitude}%2C#{destination.longitude}"

    connection = Faraday.new(url: HERE_URL)
    @response = connection.get HERE_PARAMS + "&origin=#{string_origin}&destination=#{string_destination}"
  end

  def duration_in_seconds
    return RouterResponseJSONParser.new(JSON.parse(@response.body)).duration_in_seconds if @response.success?

    nil # if response is not successful
  end
end
