class RouterResponseJSONParser
  attr_accessor :duration_in_seconds

  def initialize(response)
    route = response['routes'][0]
    section = route['sections'][0]
    summary = section['summary']
    @duration_in_seconds = summary['duration']
  rescue NoMethodError, TypeError, IndexError => _e
    @duration_in_seconds = nil
  end
end
