class CoordinateError < StandardError
  def initialize
    super('Falta especificar la latitud o la longitud!')
  end
end
