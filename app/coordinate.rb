require_relative 'coordinate_error'

class Coordinate
  attr_accessor :latitude, :longitude

  def initialize(latitude, longitude)
    raise CoordinateError if latitude.nil? || longitude.nil? || latitude == '' || longitude == ''

    @latitude = Float(latitude)
    @longitude = Float(longitude)
  end
end
