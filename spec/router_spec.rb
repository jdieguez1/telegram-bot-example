require 'web_mock'
require "#{File.dirname(__FILE__)}/../app/coordinate"
require_relative 'spec_helper'
require_relative 'router_helper'

describe Router do
  let(:origin) { Coordinate.new(37.7749, -122.4194) }
  let(:str_origin) { "#{origin.latitude}%2C#{origin.longitude}" }

  before(:each) do
    WebMock.disable_net_connect!(allow_localhost: true)
  end

  context 'when successful response' do
    let(:destination) { Coordinate.new(34.0522, -118.2437) }
    let(:str_destination) { "#{destination.latitude}%2C#{destination.longitude}" }

    it 'should return duration in seconds' do
      aux = when_i_calculate_duration_for(str_origin, str_destination)
      then_i_get_response(200, aux, File.read('here_response_example.json'))
      router = described_class.new(origin, destination)
      expect(router.duration_in_seconds).to eq(21_770)
    end
  end

  context 'when non successful response' do
    let(:another_destination) { Coordinate.new(36.0522, -118.2437) }
    let(:str_another_destination) { "#{another_destination.latitude}%2C#{another_destination.longitude}" }

    it 'should return nil if response is not successful' do
      aux = when_i_calculate_duration_for(str_origin, str_another_destination)
      then_i_get_response(400, aux, '{}')
      router = described_class.new(origin, another_destination)
      expect(router.duration_in_seconds).to be_nil
    end
  end
end
