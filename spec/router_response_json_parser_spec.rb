require 'spec_helper'

describe RouterResponseJSONParser do
  context 'with a valid response' do
    let(:valid_response) do
      {
        'routes' => [
          {
            'sections' => [
              {
                'summary' => {
                  'duration' => 123
                }
              }
            ]
          }
        ]
      }
    end

    it 'parses the duration correctly' do
      parser = described_class.new(valid_response)
      expect(parser.duration_in_seconds).to eq(123)
    end
  end

  context 'with an invalid response' do
    let(:invalid_response) { {} }

    it 'sets duration to nil' do
      parser = described_class.new(invalid_response)
      expect(parser.duration_in_seconds).to be_nil
    end
  end
end
