require 'rspec/mocks'
require 'spec_helper'

describe Trip do
  let(:origin) { Coordinate.new(10.0, 10.1) }
  let(:destination) { Coordinate.new(11.0, 11.1) }
  let(:router_mock) { instance_double('Router') }

  before(:each) do
    allow(Router).to receive(:new).with(origin, destination).and_return(router_mock)
  end

  context 'when duration_in_seconds is not nil' do
    it 'returns the duration in minutes' do
      allow(router_mock).to receive(:duration_in_seconds).and_return(120)
      trip = described_class.new(origin, destination)
      expect(trip.duration).to eq(2)
    end

    it 'returns the duration in minutes, truncating any decimal' do
      allow(router_mock).to receive(:duration_in_seconds).and_return(160)
      trip = described_class.new(origin, destination)
      expect(trip.duration).to eq(2) # Returns an Integer, so it truncates any decimal
    end
  end

  context 'when duration_in_seconds is nil' do
    it 'returns nil' do
      allow(router_mock).to receive(:duration_in_seconds).and_return(nil)
      trip = described_class.new(origin, destination)
      expect(trip.duration).to be_nil
    end
  end
end
