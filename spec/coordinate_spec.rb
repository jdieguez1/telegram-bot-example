require 'spec_helper'

describe 'Coordinate' do
  it 'creating a coordinate with valid arguments is successful' do
    coordinate = Coordinate.new(37.7749, -122.4194)
    expect(coordinate.latitude).to eq(37.7749)
    expect(coordinate.longitude).to eq(-122.4194)
  end

  it 'throws an exception when latitude is missing' do
    expect { Coordinate.new(nil, -122.4194) }.to raise_error(CoordinateError)
  end

  it 'throws an exception when longitude is missing' do
    expect { Coordinate.new(37.7749, nil) }.to raise_error(CoordinateError)
  end
end
