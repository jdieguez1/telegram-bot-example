def when_i_calculate_duration_for(origin, destination)
  url_params = "?apikey=&destination=#{destination}&origin=#{origin}&return=summary&transportMode=car"
  WebMock.stub_request(:get, "https://router.hereapi.com/v8/routes#{url_params}")
         .with(
           headers: {
             'Accept' => '*/*',
             'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
             'User-Agent' => 'Faraday v2.7.4'
           }
         )
end

def then_i_get_response(status_code, stub, response)
  stub.to_return(status: status_code, body: response, headers: {})
end
